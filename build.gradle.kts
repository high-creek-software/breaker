plugins {
    `java-library`
    idea
    kotlin("jvm")
}

group = "io.highcreeksoftware"
version = "0.1"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation("junit", "junit", "4.12")
//    implementation("io.highcreeksoftware:fortunecookie:0.1")
//    implementation(project(":fortunecookie"))

    implementation("io.javalin:javalin:3.10.1")
}
