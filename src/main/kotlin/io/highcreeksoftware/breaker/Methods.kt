package io.highcreeksoftware.breaker

import io.highcreeksoftware.breaker.exceptions.IncorrectSizeException
import java.security.SecureRandom
import java.util.*
import kotlin.experimental.xor

const val defaultTokenLength = 32
val safeMethods = arrayOf("GET", "HEAD", "OPTIONS", "TRACE")

fun mask(realToken: ByteArray): String {
    val oneTimePad = generateSecureToken()

    val xored = xorToken(oneTimePad, realToken)

    val combined = oneTimePad + xored

    return Base64.getEncoder().encodeToString(combined)
}

fun unmask(issued: ByteArray): ByteArray {
    if(issued.size != defaultTokenLength * 2) {
        throw IncorrectSizeException("The issued token is incorrectly sized.")
    }
    val oneTimePad = issued.sliceArray(0 until defaultTokenLength)
    val masked = issued.sliceArray(defaultTokenLength until issued.size)

    return xorToken(oneTimePad, masked)
}

fun xorToken(a: ByteArray, b: ByteArray): ByteArray {

    var size = a.size
    if(b.size < size) {
        size = b.size
    }

    val result: ByteArray = ByteArray(size)
    for(i in 0 until size) {
        result[i] = a[i].xor(b[i])
    }

    return result
}

fun compare(a: ByteArray, b: ByteArray): Boolean = a.contentEquals(b)

fun generateSecureToken(size: Int = defaultTokenLength): ByteArray {
    val secureRandom = SecureRandom()
    val bytes = ByteArray(size)
    secureRandom.nextBytes(bytes)

    return bytes
}