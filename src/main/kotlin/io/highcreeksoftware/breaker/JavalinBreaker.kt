package io.highcreeksoftware.breaker

import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import java.net.URI
import java.util.*

class JavalinBreaker(val cookieName: String = "breaker", val headerName: String = "X-Breaker-Value", val formName: String = "breaker_token", val ctxName: String = "breaker_token", val trustedOrigins: Array<String>? = null) {

    fun run(ctx: Context) {

        var realToken = ctx.sessionAttribute<ByteArray>(cookieName)
        if(realToken == null) {
            realToken = generateSecureToken()
            ctx.sessionAttribute(cookieName, realToken)
        }

        ctx.attribute(ctxName, mask(realToken))

        if(!safeMethods.contains(ctx.method())) {

            // Check referer & trusted origins for https
            if(ctx.scheme() == "HTTPS") {
                val referer = ctx.header("Referer") ?: throw ForbiddenResponse("Referer checking failed - no Referer.")

                val actualUri = URI(ctx.url())
                val refererUri = URI(referer)

                var valid = actualUri.scheme == refererUri.scheme && actualUri.host == refererUri.host

                if(!valid && trustedOrigins != null) {
                    for(to in trustedOrigins) {
                        if(to == refererUri.host) {
                            valid = true
                            break
                        }
                    }
                }

                if(!valid) {
                    throw ForbiddenResponse("breaker token invalid")
                }

            }

            val requestToken = requestToken(ctx) ?: throw ForbiddenResponse("breaker token unavailable")

            if(!compare(unmask(requestToken), realToken)) {
                throw ForbiddenResponse("breaker token incorrect")
            }
        }

        ctx.header("Vary", "Cookie")

    }

    private fun requestToken(ctx: Context): ByteArray? {

        var issued = ctx.header(headerName)

        if(issued == null) {
            issued = ctx.formParam(formName)
        }

        if(issued == null) {
            return null
        }

        return Base64.getDecoder().decode(issued)
    }

    fun acquireToken(ctx: Context): String? = ctx.attribute(ctxName)

    fun formField(ctx: Context): String? {
        val token = acquireToken(ctx) ?: return null

        return """<input type="hidden" name="$formName" value="$token">"""
    }
}