package io.highcreeksoftware.breaker.exceptions

import java.lang.Exception

class IncorrectSizeException(message: String): Exception(message)