package io.highcreeksoftware.breaker

import org.junit.Assert
import org.junit.Test
import java.util.*

class MethodTests {

    @Test
    fun testMaskUnmask() {
        val issued = generateSecureToken()

        val masked = mask(issued)
        val unmasked = unmask(Base64.getDecoder().decode(masked))

        Assert.assertTrue("Issued equals unmasked", compare(issued, unmasked))
    }
}