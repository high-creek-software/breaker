import java.net.URI

pluginManagement {
    repositories {
        gradlePluginPortal()
    }
    resolutionStrategy {
        eachPlugin {
            if(requested.id.id == "org.jetbrains.kotlin.jvm") {
                useModule("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.10")
            }
        }
    }
}

//sourceControl {
//    gitRepository(URI("https://gitlab.com/high-creek-software/fortune-cookie.git")) {
//        producesModule("io.highcreeksoftware:fortunecookie")
//    }
//}

//include(":fortunecookie")
//project(":fortunecookie").projectDir = File("../fortune-cookie")


rootProject.name = "breaker"

